# Copyrights mca-workshop-26
#   Author: Rostislav Epifanov
#   Created: 12/08/2020


import sys
from pathlib import Path

dirpath = Path(__file__).parent

for path in dirpath.glob('[!__]*'):
    if path.is_dir():
        sys.path.insert(0, str(path))

