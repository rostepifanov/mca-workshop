# Copyrights mca-workshop-26
#   Author: Maria Maslova
#   Created: 12/08/2020

import typing as ty

from algorithm.components.token_transformer import TokenTransformer

class LowercaseTokenTransformwer(TokenTransformer):
    def forward(self, tokens: ty.List[str]) -> ty.List[str]:
        lowercased = []
        for token in tokens:
            lowercased.append(token.lower())

        return lowercased
