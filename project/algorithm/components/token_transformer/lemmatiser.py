# Copyrights mca-workshop-26
#   Author: Vasily Afanasyev
#   Created: 12/08/2020

import typing as ty
from pymystem3 import Mystem

from algorithm.components.token_transformer import TokenTransformer

class Lemmatiser(TokenTransformer):
    def __init__(self):
        self.__mystem = Mystem() 


    def forward(self, tokens: ty.List[str]) -> ty.List[str]:
        ntokens = list()

        for token in tokens:
            try:
                pr = self.__mystem.analyze(token)[0]
                ntokens.append(pr["analysis"][0]["lex"])
            except:
                ntokens.append(token)

        return ntokens

