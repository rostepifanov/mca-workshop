# Copyrights mca-workshop-26
#   Author: Rostislav Epifanov
#   Created: 10/08/2020


import typing as ty

from abc import abstractmethod


class TokenTransformer(object):
    def __init__(self, *args, **kwargs):
        pass

    @abstractmethod
    def forward(self, tokens: ty.List[str]) -> ty.List[str]:
        raise NotImplementedError('Not implemented TokenTransformer forward method')
