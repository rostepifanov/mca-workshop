# Copyrights mca-workshop-26
#   Author: Vasily Afanasyev
#   Created: 12/08/2020

import typing as ty
import re

from algorithm.components.tokenizer import Tokenizer

class SpaceDivider(Tokenizer):
    def __init__(self):
        pass

    def forward(self, text: str) -> ty.List[str]:
        while text[0] == ' ':
            text = text[1:]

        while text[len(text)-1] == ' ':
            text = text[:-1]

        tokens: ty.List[str] = re.split(r'\s+', text)
            
        return tokens

