# Copyrights mca-workshop-26
#   Author: Rostislav Epifanov
#   Created: 10/08/2020


import typing as ty

from abc import abstractmethod


class Tokenizer(object):
    def __init__(self, *args, **kwargs):
        pass

    @abstractmethod
    def forward(self, text: str) -> ty.List[str]:
        raise NotImplementedError('Not implemented Tokenizer forward method')
