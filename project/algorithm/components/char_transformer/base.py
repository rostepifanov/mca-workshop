# Copyrights mca-workshop-26
#   Author: Rostislav Epifanov
#   Created: 10/08/2020

from abc import abstractmethod


class CharTransformer(object):
    def __init__(self, *args, **kwargs):
        pass

    @abstractmethod
    def forward(self, text: str) -> str:
        raise NotImplementedError('Not implemented CharTransformer forward method')

