# Copyrights mca-workshop-26
#   Author: Rostislav Epifanov
#   Created: 11/08/2020

import re

from algorithm.components.char_transformer import CharTransformer


class LowercaseCharTransformer(CharTransformer):
    def forward(self, text: str) -> str:
        text = text.lower()

        return text
