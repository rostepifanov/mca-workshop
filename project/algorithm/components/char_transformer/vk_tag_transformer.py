# Copyrights mca-workshop-26
#   Author: Vasily Afanasyev
#   Created: 11/08/2020

import re

from algorithm.components.char_transformer import CharTransformer


class VkTagTransformer(CharTransformer):
    def __init__(self, transformer):
        self.__tag_template = r"(?P<tag>\[(id|club)\d+\|\w+\],)(?P<text>.+)"
        self.__transformer = transformer

    def forward(self, text: str) -> str:
        match = re.match(self.__tag_template, text)

        if match:
            text = self.__transformer(match.group('tag')) + match.group('text')

        return text

