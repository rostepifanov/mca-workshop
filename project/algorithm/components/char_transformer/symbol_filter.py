# Copyrights mca-workshop-26
#   Author: Maria Maslova
#   Created: 11/08/2020

import string

from abc import abstractmethod

from algorithm.components.char_transformer import CharTransformer


class SymbolFilter(CharTransformer):
    def __init__(self, punctuation=string.punctuation):
        self.__punctuation = punctuation

    def forward(self, text: str) -> str:
        for symbol in self.__punctuation:
            text = text.replace(symbol, '')
        return text

