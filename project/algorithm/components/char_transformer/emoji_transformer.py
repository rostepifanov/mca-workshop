# Copyrights mca-workshop-26
#   Author: Dmitry Morozov
#   Created: 11/08/2020

import re
from pathlib import Path

try:
    import cPickle as pickle
except ImportError:
    import pickle
import re

from algorithm.components.char_transformer import CharTransformer


class EmojiTransformer(CharTransformer):
    def __init__(self, replace=True):
        projpath = Path(__file__).parent.parent.parent
        model_dirpath = projpath / 'thirdparty' / 'spacymoji'

        with open(model_dirpath / 'Emoji_Dict.p', 'rb') as fp:
            Emoji_Dict = pickle.load(fp)

        self.emoji_dict = {v: k for k, v in Emoji_Dict.items()}

        with open(model_dirpath / 'Emoticon_Dict.p', 'rb') as fp:
            self.emoticon_dict = pickle.load(fp)

        self.__replace = replace

    def forward(self, text: str) -> str:
        return self.__remove_emoticons(self.__convert_emojis_to_word(text))

    def __convert_emojis_to_word(self, text):
        for emot in self.emoji_dict:
            if self.__replace:
                text = re.sub(r'(' + emot + ')', "_".join(self.emoji_dict[emot].replace(",", "").replace(":", "").split()), text)
            else:
                text = re.sub(r'(' + emot + ')', '', text)
        return text

    def __remove_emoticons(self, text):
        emoticon_pattern = re.compile(u'(' + u'|'.join(k for k in self.emoticon_dict) + u')')
        return emoticon_pattern.sub(r'', text)
