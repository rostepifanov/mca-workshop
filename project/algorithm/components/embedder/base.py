# Copyrights mca-workshop-26
#   Author: Rostislav Epifanov
#   Created: 10/08/2020


import numpy as np
import typing as ty

from abc import abstractmethod


class Embedder(object):
    def __init__(self, *args, **kwargs):
        pass

    @abstractmethod
    def forward(self, tokens: ty.List[str]) -> ty.List[np.array]:
        raise NotImplementedError('Not implemented Embedder forward method')
