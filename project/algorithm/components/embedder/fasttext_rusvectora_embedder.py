# Copyrights mca-workshop-26
#   Author: Leonid Motovskikh
#   Created: 12/08/2020


import gensim
import numpy as np
import typing as ty

from algorithm.components.embedder import Embedder


class FasttextRusvectoraEmbedder(Embedder):
    def __init__(self, path: str):
        self.__model = gensim.models.KeyedVectors.load(path)

        self.dim = self.__model.wv.vectors.shape[1]

    def forward(self, tokens: ty.List[str]) -> ty.List[np.array]:
        return [self.__word_vec(token) for token in tokens]

    def __word_vec(self, token: str) -> np.array:
        try:
            return self.__model.word_vec(token)
        except KeyError:
            return np.empty(self.__model.vectors.shape[1])
