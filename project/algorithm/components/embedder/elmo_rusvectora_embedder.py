# Copyrights mca-workshop-26
#   Author: Rostislav Epifanov
#   Created: 11/08/2020


import json
import typing as ty
import numpy as np
import tensorflow as tf

from pathlib import Path

from algorithm.components.embedder import Embedder
from algorithm.thirdparty.simple_elmo.elmo_helpers import load_elmo_embeddings, get_elmo_vectors

class ElmoRusvectoraEmbedder(Embedder):
    def __init__(self, elmo_path):
        self.__batcher, self.__sentence_character_ids, self.__elmo_sentence_input = load_elmo_embeddings(elmo_path)

        self.__init_metainfo(elmo_path)
        self.__init_tf_env()

    def __init_metainfo(self, elmo_path):
        with open(Path(elmo_path) / 'meta.json', 'r') as f:
            metainfo = json.load(f)

        self.dim = metainfo['dimensions']

    def __init_tf_env(self):
        self.__sess = tf.Session()
        init = tf.global_variables_initializer()
        init.run(session=self.__sess)

    def forward(self, tokens: ty.List[str]) -> ty.List[np.array]:
        elmo_vectors = get_elmo_vectors( self.__sess,
                                         [tokens],
                                         self.__batcher,
                                         self.__sentence_character_ids,
                                         self.__elmo_sentence_input )

        return elmo_vectors[0][:len(tokens), :]
