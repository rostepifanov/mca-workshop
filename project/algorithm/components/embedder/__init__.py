# Copyrights mca-workshop-26
#   Author: Rostislav Epifanov
#   Created: 10/08/2020

from algorithm.components.embedder.base import Embedder
from algorithm.components.embedder.elmo_rusvectora_embedder import ElmoRusvectoraEmbedder
from algorithm.components.embedder.fasttext_rusvectora_embedder import FasttextRusvectoraEmbedder
