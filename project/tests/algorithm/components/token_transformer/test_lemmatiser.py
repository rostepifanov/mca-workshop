# Copyrights mca-workshop-26
#   Author: Vasily Afanasyev
#   Created: 12/08/2020

import pytest
import typing as ty

from algorithm.components.token_transformer import Lemmatiser

class TestLemmatiser(object):
    def test_lemmatiser_CASE_basic(self):
        lem = Lemmatiser()
        words: ty.List[str] = ["надели", "Медведи", "шляпу", "как", "Раз"]
        lem_words: ty.List[str] = ["надевать", "медведь", "шляпа", "как", "раз"]
        assert lem.forward(words) == lem_words

