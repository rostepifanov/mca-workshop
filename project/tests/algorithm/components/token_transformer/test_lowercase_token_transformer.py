# Copyrights mca-workshop-26
#   Author: Maria Maslova
#   Created: 12/08/2020

import pytest

from algorithm.components.token_transformer import LowercaseTokenTransformwer

@pytest.mark.token_transformer
def test_lowercase_token_transformer_CASE_default_behaviour():
    transformer = LowercaseTokenTransformwer()

    tokens = ['почему', 'Мне', 'БЛИН', 'нИ', 'фига', 'НЕ', 'пОнятно']
    output = transformer.forward(tokens)

    expected = ['почему', 'мне', 'блин', 'ни', 'фига', 'не', 'понятно']

    assert output == expected
