# Copyrights mca-workshop-26
#   Author: Vasily Afanasyev
#   Created: 12/08/2020

import pytest
import typing as ty

from algorithm.components.tokenizer.space_divider import SpaceDivider


class TestSpaceDivider(object):
    def test_space_divider_CASE_basic(self):
        ctt = SpaceDivider()
        words: ty.List[str] = ["text", "hello", "there", "hehehe", "comment", "bruh"]
        assert ctt.forward("text hello there hehehe comment bruh") == words

    def test_space_divider_CASE_multiple_spaces(self):
        ctt = SpaceDivider()
        words: ty.List[str] = ["text", "hello", "there", "hehehe", "comment", "bruh"]
        assert ctt.forward("text hello there hehehe comment   bruh") == words
    
    def test_space_divider_CASE_one_word(self):
        ctt = SpaceDivider()
        words: ty.List[str] = ["bruh"]
        assert ctt.forward("bruh") == words
    
    def test_space_divider_CASE_starts_with_space(self):
        ctt = SpaceDivider()
        words: ty.List[str] = ["bruh", "boiii"]
        assert ctt.forward("  bruh  boiii") == words
    
    def test_space_divider_CASE_ends_with_space(self):
        ctt = SpaceDivider()
        words: ty.List[str] = ["bruh", "boiii"]
        assert ctt.forward("bruh  boiii   ") == words

