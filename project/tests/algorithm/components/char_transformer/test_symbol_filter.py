# Copyrights mca-workshop-26
#   Author: Maria Maslova
#   Created: 11/08/2020


import pytest

from algorithm.components.char_transformer import SymbolFilter

@pytest.mark.char_transformer
def test_symbol_filter_CASE_default_behaviour():
    transformer = SymbolFilter()

    text = 'почему мне, блин, ни фига не понятно?!'

    output = transformer.forward(text)
    expected = 'почему мне блин ни фига не понятно'

    assert output == expected


@pytest.mark.char_transformer
def test_symbol_filter_CASE_letter_filtration_behaviour():
    transformer = SymbolFilter(['а','б','н'])

    text = 'почему мне, блин, ни фига не понятно?!'

    output = transformer.forward(text)
    expected = 'почему ме, ли, и фиг е поято?!'

    assert output == expected

