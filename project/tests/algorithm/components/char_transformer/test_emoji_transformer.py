# Copyrights mca-workshop-26
#   Author: Dmitry Morozov
#   Created: 11/08/2020

import pytest

from algorithm.components.char_transformer import EmojiTransformer


@pytest.mark.char_transformer
def test_emoji_transformer_CASE_emoji():
    ctt = EmojiTransformer()
    assert ctt.forward("I won 🥇 in 🏏") == "I won 1st_place_medal in cricket"


@pytest.mark.char_transformer
def test_emoji_transformer_CASE_emoticon_1():
    ctt = EmojiTransformer()
    assert ctt.forward("Good Morning :-)") == "Good Morning "


@pytest.mark.char_transformer
def test_emoji_transformer_CASE_emoticon_2():
    ctt = EmojiTransformer()
    assert ctt.forward("Good Morning:)") == "Good Morning"

