# Copyrights mca-workshop-26
#   Author: Rostislav Epifanov
#   Created: 11/08/2020

import pytest

from algorithm.components.char_transformer import LowercaseCharTransformer


@pytest.mark.char_transformer
def test_lowercase_char_transformer_CASE_simple_text():
    transformer = LowercaseCharTransformer()

    text = 'Lorem ipsum dolor sit amet.'

    output = transformer.forward(text)
    expected = 'lorem ipsum dolor sit amet.'

    assert output == expected

