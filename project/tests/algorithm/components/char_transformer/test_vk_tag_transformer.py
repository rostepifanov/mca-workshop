# Copyrights mca-workshop-26
#   Author: Vasily Afanasyev
#   Created: 11/08/2020

import pytest

from algorithm.components.char_transformer import VkTagTransformer


def transformer(tag: str) -> str:
    tag = "tag"
    return tag

@pytest.mark.char_transformer
def test_vk_tag_transformer_CASE_has_id():
    ctt = VkTagTransformer(transformer)
    assert ctt.forward("[id12345|UserName], some comment") == "tag some comment"

@pytest.mark.char_transformer
def test_vk_tag_transformer_CASE_has_club():
    ctt = VkTagTransformer(transformer)
    assert ctt.forward("[club54321|ClubName], another comment") == "tag another comment"

@pytest.mark.char_transformer
def test_vk_tag_transformer_CASE_without():
    ctt = VkTagTransformer(transformer)
    assert ctt.forward("third comment") == "third comment"

