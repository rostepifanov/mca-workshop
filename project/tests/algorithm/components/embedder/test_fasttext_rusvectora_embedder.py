# Copyrights mca-workshop-26
#   Author: Leonid Motovskikh
#   Created: 12/08/2020

import os
from pathlib import Path

import pytest

from algorithm.components.embedder import FasttextRusvectoraEmbedder 


def __model_availability():
    model_path = os.getenv('ALGORITHM_FASTTEXT_PATH', '')
    model_path = Path(model_path)

    return not model_path.is_file()


@pytest.mark.embedder
@pytest.mark.skipif(__model_availability(), reason='No fasttext model')
def test_fasttext_rusvectora_embedder_CASE_shape_equality():
    embedder = FasttextRusvectoraEmbedder(os.getenv('ALGORITHM_FASTTEXT_PATH'))

    tokens = ['один', 'два', 'три']
    output = embedder.forward(tokens)

    assert len(tokens) == len(output)

    for out in output:
        assert out.shape == (embedder.dim, )
