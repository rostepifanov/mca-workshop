# Copyrights mca-workshop-26
#   Author: Rostislav Epifanov
#   Created: 11/08/2020

import pytest

import os
from pathlib import Path

from algorithm.components.embedder import ElmoRusvectoraEmbedder


def __model_availability():
    elmo_path = os.getenv('ALGORITHM_ELMO_PATH', '')
    elmo_path = Path(elmo_path)

    return not (      elmo_path.is_dir()
                 and (elmo_path / 'model.hdf5').is_file()
                 and (elmo_path / 'options.json').is_file()
                 and (elmo_path / 'meta.json').is_file()
                 and (    (elmo_path / 'vocab.txt').is_file()
                       or (elmo_path / 'vocab.txt.gz').is_file()))

@pytest.mark.embedder
@pytest.mark.skipif(__model_availability(), reason='No elmo model')
def test_elmo_rusvectora_embedder_CASE_shape_equality():
    embedder = ElmoRusvectoraEmbedder(os.getenv('ALGORITHM_ELMO_PATH'))

    tokens = ['один', 'два', 'три']
    output = embedder.forward(tokens)

    assert len(tokens) == len(output)

    for out in output:
        assert out.shape == (embedder.dim, )
