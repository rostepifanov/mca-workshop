		***Статистика по датасету**

====Файл train.csv (обучающая выборка):

==Количество примеров:	3228

==Количество меток:

0           2794
1            426
unmarked       7
skipped        1
Name: decent, dtype: int64

0                  2885
1                   325
skipped              12
unmarked              6
Name: moral, dtype: int64

0           2756
1            463
unmarked       4
skipped        3
Name: person, dtype: int64

==Количество оскорблений (совпадение по 3 меткам):	38

====Файл test.csv (тестовая выборка):

==Количество примеров:	1384

==Количество меток:

0           1227
1            155
unmarked       1
skipped        1
Name: decent, dtype: int64

0           1254
1            124
skipped        5
unmarked       1
Name: moral, dtype: int64

0          1207
1           174
skipped       2
Name: person, dtype: int64

==Количество оскорблений (совпадение по 3 меткам):	13